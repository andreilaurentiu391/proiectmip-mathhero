import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public abstract class ListeningGameComponent extends GameComponent implements KeyListener {
    public ArrayList<String> keysPressed = new ArrayList<>();
    public boolean debug = false;

    public ListeningGameComponent(int w, int h) {
        super(w, h);
        addKeyListener(this);
    }

    public abstract void draw(Graphics g);

    public abstract void update();


    public void keyPressed(KeyEvent e) {
        if (debug)
            System.out.println(KeyEvent.getKeyText(e.getKeyCode()));
        keysPressed.add(KeyEvent.getKeyText(e.getKeyCode()));
    }

    public void keyReleased(KeyEvent e) {
        for (int i = 0; i < keysPressed.size(); i++) {
            if (keysPressed.get(i).equals(KeyEvent.getKeyText(e.getKeyCode()))) {
                keysPressed.remove(i);
                i--;
            }
        }
    }

    public void keyTyped(KeyEvent e) {
    }

}